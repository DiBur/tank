﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1
{
    //данные динамического объекта, тип Guid - тип уникальных идентификаторов — 16-байтных значений
    //more on https://blog.rc21net.ru/csharp-guid/ 
    public abstract class BaseDynamicObject
    {
        public Guid Id { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public BaseDynamicObject(int x, int y)
        {
            X = x;
            Y = y;

            Id = Guid.NewGuid();
        }
    }
}