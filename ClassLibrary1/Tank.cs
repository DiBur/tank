﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1
{
    //параметры объектов (танк)
    public class Tank: MovingDynamicObject
    {
        public string Tag { get; set; }//подпись, верно? На ней должны указываться координаты и HP?
        public int MaxHP { get; set; }
        public int CurrentHP { get; set; }
        public Tank( int x, int y, string tag) : base(x, y, DirectionType.Left, false, 1)
        {
            Tag = tag;
            MaxHP = CurrentHP = 100;

        }
    }
}
