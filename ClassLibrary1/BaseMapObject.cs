﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1
{
    //данные статического объекта, в коде сервера, кажется, не использовались, т.к. там X и Y из BaseDynamicObject. 
    //Далее понадобятся?
    public abstract class BaseMapObject
    {
        public int X;
        public int Y;
        public int Width;
        public int Height;
    }
}
