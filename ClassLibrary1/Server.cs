﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Tank.Common.Enums;
using Tank.Common.Objects;
using TankObject = Tank.Common.Objects.Tank;
using Tank.Common.GameSpace;

namespace ClassLibrary1
{
    /// <summary>
    /// класс динамических объектов (танк, снаряд, расходники, пр). [новые динам. объекты записываются сюда?]
    /// </summary>
    public class DynamicObject : List<BaseDynamicObject>
    {
    }

    /// <summary>
    /// Инициализирование экземпляра "ClientDelegate".
    /// </summary>
    /// <param name="map"></param>
    /// Карта
    /// <param name="dynamicObjects"></param>
    /// <returns></returns>
    public delegate ClientCommandType ClientDelegate(Map map, DynamicObject dynamicObjects);

    public class Server
    {
        /// <summary>
        /// Создание объекта типа MapInfo, т.е. который будет проверять на условие, но в какой момент?
        /// </summary>
        public Map Map;
        /// <summary>
        /// создание объекта типа "DynamicObjects". (Разве мы не должны были указать тип "BaseDynamicObject"? Как на это повлияли строки 10-13? В чем идея?)
        /// </summary>
        public DynamicObject DynamicObjects;
        /// <summary>
        /// создание списка объектов "Clients" под типом "ClientDelegate"
        /// </summary>
        public List<KeyValuePair<ClientDelegate, MovingDynamicObject>> Clients;

        public void Run()
        {
            List<KeyValuePair<MovingDynamicObject, ClientCommandType>> commands = new List<KeyValuePair<MovingDynamicObject, ClientCommandType>>();
            foreach( var client in Clients)
            {
                var cmd = client.Key(Map, DynamicObjects);
                commands.Add(new KeyValuePair<MovingDynamicObject, ClientCommandType> (client.Value, cmd));
            }
            foreach (var command in commands)
            {
                switch (command)
                {
                    /*case command = ClientCommandType.Go:
                        if (SellType=())
                        MovingDynamicObject.IsRun = true;
                        Speed = 1;
                        break
                    case command = ClientCommandType.Stop:
                        MovingDynamicObject.IsRun = false;
                        Speed = 0;
                        break
                    case ClientCommandType.TurnDown:
                        DirectionType = */
                }
                /*
                client.Key(...);
                switch (...)
                case LeftTurn: tank.Direction = Direction.Left;
                case ...
                case Go: tank.IsRun = true;
                */
            }

        }

            /*
            1 сбор команд
            2 проверка на возможность выполнения
            3 если возможно - выполнение
            3 если невозможно - through exeption
            */


        private Random _random = new Random(); //создание типа _random для присвоения случайных значений

        //создание клиента для объекта "танк"
        public TankObject AddClient(ClientDelegate client, string tag) 
        {
            if (client == null)//проверка на отсутствие клиента 
            {
                throw new ArgumentNullException(nameof(client));//при отсутствующем клиенте кидается exeption(на имя этого клиента? забыл)
            }

            int x, y;
            

            while (true)
            {
                x = _random.Next(Map.MapWidth);//присвоение переменным случайного значения в рамках ширины и высоты карты
                y = _random.Next(Map.MapHeight);

                var type = Map[x, y];

                switch ( type ) //при попадании рандома на эти типы клеток заставляем продолжать искать 
                {               //(производится возвращение в начало цикла или же создания клиента?)
                    case CellType.Wall:
                    case CellType.Water:
                        continue;
                }

                //умный, быстрый (как называется этот способ?)
                if(DynamicObjects.Any(obj => obj.X == x && obj.Y == y))//при совпадении координат с координатами любого динамического 
                {                                                       //объекта - продолжение поиска
                    continue;
                }

//то же самое, что и выше, но больше кода, больше возможностей ошибиться
/*
var blocked = false; //флаг
foreach (var obj in DynamicObjects) //для каждого объекта в 
{
    if ((obj.X == x) && (obj.Y == y)) //случайные координаты совпадают с координатами какого-либо динамического объекта?
    {
        blocked = true; //поднять флаг!
        break; //прекращение цикла "while"
    }
}

if (blocked) //при поднятом флаге начать с начала (так ведь?)
{
    continue;
}
*/

                break; //прекращение цикла "while"
            }
            var result = new TankObject (x, y, tag);//создание объекта класса "Танк"
            DynamicObjects.Add(result); //добавление нового объекта "Танк" в список динамических объектов
            Clients.Add(new KeyValuePair<ClientDelegate, MovingDynamicObject> (client, result));//создание клиента
            return result; //возвращение объекта

        }

        /// <summary>
        /// инициализирование карты
        /// </summary>
        public void MapInit()
        {
            Map = new Map(); //создание объекта "Map"
            var mapContent = File.ReadAllLines("Map.txt"); //в перуменную "mapContent" считывается пресозданная карта из файла

            foreach (var line in mapContent) //для каждой строки в переданном файле выполняется
            {
                var mapLine = new List<CellType>(); //создается переменная типа список клеток 
                Map.Add(mapLine); // в объект карта добавляется список клеток

                foreach (var symbol in line) //для каждого символа в строке
                {
                    switch (symbol) //производится посимвольное заполнение списка клеток
                    {
                        case ' ': //в зависимости от символа в .txt файле "Map"
                            mapLine.Add(CellType.Empty);
                            break;
                        case 'с':
                            mapLine.Add(CellType.Wall);
                            break;
                        case 'в':
                            mapLine.Add(CellType.Water);
                            break;
                        case 'т':
                            mapLine.Add(CellType.Grass);
                            break;
                        default:
                            throw new Exception("Нет такой ячейки");

                    }
                }
            }
        }
    }
}