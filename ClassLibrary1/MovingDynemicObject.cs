﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1
{
    public abstract class MovingDynamicObject : BaseDynamicObject
    {
        protected MovingDynamicObject(int x, int y, DirectionType direction, bool isRun, int speed) : base (x, y)
        {
            Direction = direction;
            IsRun = isRun;
            Speed = speed;
        }

        public DirectionType Direction { get; set; }
        public bool IsRun { get; set; }
        public int Speed { get; set; }

    }
}
