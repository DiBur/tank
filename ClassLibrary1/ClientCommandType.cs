﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1
{
    //команды клиента для изменения состаяния танка (мб не только) 
    public enum ClientCommandType
    {
        None,
        Go,
        Stop,
        TurnRight,
        TurnLeft,
        TurnUp,
        TurnDown,
        Fire,
        Die
    }
}
