﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1
{
    /// <summary>
    /// Тип ячейки
    /// </summary>
    public enum CellType
    { 
        /// <summary>
        /// Пустая
        /// </summary>
        Empty, 
        /// <summary>
        /// Стена
        /// </summary>
        Wall,
        /// <summary>
        /// Вода
        /// </summary>
        Water,
        /// <summary>
        /// Трава
        /// </summary>
        Grass
        
    }

}
