﻿using System;
using System.Collections.Generic;
using Tank.Common.Enums;
using Tank.Common.Objects;

namespace Tank.Common.GameSpace
{
    /// <summary>
    /// Класс, в котором проводится проверка на соответствие координат объекта координатам поля.
    /// Наследует свойства списка списков типа "CellType".
    /// </summary>
    public class Map : List<List<CellType>>
    {
        #region Fields

        /// <summary>
        /// это Х, ширина
        /// </summary>
        public int MapWidth => Cells.GetLength(1);
        /// <summary>
        /// это Y, высота
        /// </summary>
        public int MapHeight => Cells.GetLength(0);

        public CellType[,] Cells; 

        public List<BaseDynamicObject> DynamicObjects;

        #endregion Fields

        public Map(CellType[,] cells)
        {
            Cells = cells;
            DynamicObjects = new List<BaseDynamicObject>();
        }

        public Map(Map map,List<BaseDynamicObject> objects)
        {
            DynamicObjects = objects;
            Cells = (CellType[,])map.Cells.Clone();
        }

        public CellType this[int x, int y] //проверка на соответствие
        {
            /// <summary>
            /// [что делает get? Значения X и Y какого объекта и типа получает? ]
            ///  </summary>
            get
            {
                if ((x < 0) || (x > MapWidth))
                {
                    /// <summary>
                    /// исключение
                    /// </summary>
                    throw new IndexOutOfRangeException();
                }
                if ((y < 0) || (y > MapHeight))
                {
                    throw new IndexOutOfRangeException();
                }
                /// <summary>
                ///возврат значения
                /// </summary>
                return this[y][x];
            }
        }
    }
}
