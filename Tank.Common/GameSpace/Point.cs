﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tank.Common.GameSpace
{
    public class Point
    {
        public decimal X;
        public decimal Y;

        public Point(decimal x, decimal y)
        {
            X = x;
            Y = y;
        }

        public Point(Point point)
        {
            X = point.X;
            Y = point.Y;
        }

        public override int GetHashCode()
        {
            return X.GetHashCode()*Y.GetHashCode();
        }

        public override string ToString()
        {
            return $"X: {X}, Y: {Y};";
        }

        public override bool Equals(object obj)
        {
            return ((obj == null) || !(obj is Point p))
                ? false
                : ((p.X == X) && (p.Y == Y));
        }
    }
}
