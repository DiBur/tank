﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tank.Common.Objects
{
    // Данные динамического объекта, тип Guid - тип уникальных идентификаторов — 16-байтных значений.
    // More on https://blog.rc21net.ru/csharp-guid/.
    /// <summary>
    /// Абстрактный класс для динамических объектов.
    /// </summary>
    public abstract class BaseDynamicObject
    {
        public Guid Id { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        /// <summary>
        /// Инициализация полей класса.
        /// </summary>
        /// <param name="x"></param>
        /// Координаты по Х.
        /// <param name="y"></param>
        /// Координаты по Y
        public BaseDynamicObject(int x, int y)
        {
            X = x;
            Y = y;

            Id = Guid.NewGuid();
        }
    }
}