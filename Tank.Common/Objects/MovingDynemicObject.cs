﻿using Tank.Common.Enums;

namespace Tank.Common.Objects
{
    /// <summary>
    /// Класс, наследуемый от BaseDynamicObject, для движущихся объектов (танк, пуля)
    /// </summary>
    public abstract class MovingDynamicObject : BaseDynamicObject
    {
        /// <summary>
        /// Конструктор движущихся объектов
        /// </summary>
        /// <param name="x"></param>
        /// Координаты по Х, берется из BaseDynamicObject.
        /// <param name="y"></param>
        /// Координаты по Y, берется из BaseDynamicObject.
        /// <param name="direction"></param>
        /// Направление движения, берется из BaseDynamicObject.
        /// <param name="isRun"></param>
        /// Флаг движения, берется из BaseDynamicObject.
        /// <param name="speed"></param>
        /// Параметр скорости, берется из BaseDynamicObject.
        protected MovingDynamicObject(int x, int y, DirectionType direction, bool isRun, int speed) : base (x, y)
        {
            Direction = direction;
            IsRun = isRun;
            Speed = speed;
        }

        public DirectionType Direction { get; set; }
        public bool IsRun { get; set; }
        public int Speed { get; set; }

    }
}
