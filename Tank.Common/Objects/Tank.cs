﻿using Tank.Common.Enums;

namespace Tank.Common.Objects
{
    ///<summary>
    ///параметры объектов (танк)
    /// </summary>
    public class Tank: MovingDynamicObject
    {
        //подпись, верно? На ней должны указываться координаты и HP?
        public string Tag { get; set; }
        public int MaxHP { get; set; }
        public int CurrentHP { get; set; }
        public Tank( int x, int y, string tag) : base(x, y, DirectionType.Left, false, 1)
        {
            Tag = tag;
            MaxHP = CurrentHP = 100;

        }
    }
}
