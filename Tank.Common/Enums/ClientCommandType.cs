﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tank.Common.Enums
{
    public enum ClientCommandType
    {
        None,
        Go,
        Stop,
        TurnLeft,
        TurnRight,
        TurnUp,
        TurnDown,
        Fire,
        Die
    }
}
