﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tank.Common.Enums
{
    /// <summary>
    /// Перечисление возможных направлений
    /// </summary>
    public enum DirectionType
    {
        /// <summary>
        /// Влево
        /// </summary>
        Left,
        /// <summary>
        /// Вправо
        /// </summary>
        Right,
        /// <summary>
        /// Вверх
        /// </summary>
        Up,
        /// <summary>
        /// Вниз
        /// </summary>
        Down
    }
}
